#include <Servo.h>

#include <stdint.h>
#include <string.h>

#include "PinDefinitionsAndMore.h"
#include <IRremote.hpp> // include the library

#define DECODE_NEC  

/**
 * \brief use to define type of message with motor
 */
typedef enum
{
    MSG_NONE = 0x00,
    MSG_VERIFY = 0x01,
    MSG_MOTOR_SPEED = 0x02,
    MSG_MOTOR_STOP = 0x03,
    MSG_MOTOR_PING = 0x05,
    MSG_MOTOR_SHOOT = 0x06,
    MSG_MOTOR_HIT = 0x07
} Type_of_message;

/**
 * \brief use to notify when we need to update motor speed
 */
bool update_motor_speed = true;

/**
 * \brief buffer receive of arduino 
 */
uint8_t buffer_receive[256];

/**
 * \brief idx of array of buffer_receive (count of octet are in)
 */
uint8_t buffer_receive_idx;

/**
 * \brief variable use to calculate CRC based on this polynomial
*/
const uint16_t Polynomial = 0xA001;

/**
 * \brief array of corresponding for calculate CRC 16
*/
uint16_t table_crc[256];

/**
 * \brief pin of left motor on arduino
 */
const uint8_t pin_left_motor = 13;

/**
 * \brief pin of right motor on arduino
 */
const uint8_t pin_right_motor = 12;

/**
 * \brief value to attach to left motor to disable rotation
 */
 //96
const int16_t no_rotation_left_motor = 93;

/**
 * \brief value to attach to right motor to disable rotation
 */
 //74
const int16_t no_rotation_right_motor = 90;

/**
 * \brief current speed of left motor
 */
int16_t speed_left = no_rotation_left_motor;

/**
 * \brief current speed of right motor
 */
int16_t speed_right = no_rotation_right_motor;

/**
 * \brief class use to access to left motor
 */
Servo motor_left;

/**
 * \brief class use to access to right motor
 */
Servo motor_right;

const int ledPin = 6;

/**
 * \brief setup one time on arduino launch
 */
void setup() 
{
  //initialize serial port
  Serial.begin(115200);

  pinMode(ledPin, OUTPUT);

  //connectin motor with pin on arduino
  motor_left.attach(pin_left_motor);
  motor_right.attach(pin_right_motor);

  IrReceiver.begin(IR_RECEIVE_PIN, ENABLE_LED_FEEDBACK);

  led_on();

  init_crc_16();
  memset(buffer_receive, 0, sizeof(uint8_t) * sizeof(buffer_receive));
  buffer_receive_idx = 0;
}

/**
 * \brief main loop  of arduino
 */
void loop() 
{
  //if we receive data on serial port
  if (Serial.available() > 0) 
  {
    buffer_receive_idx += Serial.readBytes(buffer_receive + buffer_receive_idx, Serial.available() + buffer_receive_idx > 255 ? 0 : Serial.available());
    parse_frame();
  }
  //updating speed
  if(update_motor_speed == true)
  {
    motor_left.write(speed_left);
    motor_right.write(speed_right);
    update_motor_speed = false;
  }

  check_if_hit();

   

  delay(10);
}

void led_on(void)
{
  // LED à 39.2 %. 
  analogWrite(ledPin, 100);
}

void led_off(void)
{
  analogWrite(ledPin, 0);
}

void check_if_hit(void)
{
  if (IrReceiver.decode()) 
  {
      if (IrReceiver.decodedIRData.protocol == UNKNOWN) 
      {
        IrReceiver.printIRResultRawFormatted(&Serial, true);
        IrReceiver.resume();
      } 
      else 
      {
        IrReceiver.resume(); 
        IrReceiver.printIRResultShort(&Serial);
        IrReceiver.printIRSendUsage(&Serial);
      }
      Serial.println();

      if (IrReceiver.decodedIRData.command == 0x31) 
      {
        Serial.println("On tir");
        led_off();
        send_hit();
      }
  }
}

void parse_frame(void)
{
  if (buffer_receive_idx == 0)
        return;

    bool start_of_message = false;
    uint8_t i = 0;

    for (; i < buffer_receive_idx; ++i)
    {
        if (buffer_receive[i] == 0x01 && start_of_message == false)
        {
            start_of_message = true;
            break;
        }
        else if (start_of_message == false)
            continue;
    }

    uint8_t size_of_msg = 0;
    Type_of_message type_of_msg = MSG_NONE;

    if (buffer_receive_idx - 3 >= i)
    {
        memcpy(&size_of_msg, &buffer_receive[i + 2], sizeof(uint8_t));
        memcpy(&type_of_msg, &buffer_receive[i + 1], sizeof(uint8_t));
    }

    if (size_of_msg + i > buffer_receive_idx)
        return;

    uint16_t crc_receive = 0;
    memcpy(&crc_receive, buffer_receive + i + 3 + size_of_msg - sizeof(uint16_t), sizeof(uint16_t));
    uint16_t crc_compute = compute_checksum(buffer_receive + i, size_of_msg + 1);

    if (crc_compute != crc_receive)
    {
        uint8_t msg[6] = {0x01, MSG_VERIFY, 0x03, 0x00, 0x00, 0x00};
        uint16_t crc_to_send = compute_checksum(msg, 4);
        memcpy(msg + 4, &crc_to_send, sizeof(uint16_t));
        Serial.write(msg, 6);
        
        memset(buffer_receive, 0, 255);
        buffer_receive_idx = 0;
        return;
    }

    switch(type_of_msg)
    {
      case MSG_VERIFY:
        {
          uint8_t msg[6] = {0x01, MSG_VERIFY, 0x03, 0x01, 0x00, 0x00};
          if(buffer_receive[i + 3] != 0x01)
            msg[3] = 0x00;

          uint16_t crc_to_send = compute_checksum(msg, 4);
          memcpy(msg + 4, &crc_to_send, sizeof(uint16_t));
          Serial.write(msg, 6);
        }
        break;
      case MSG_MOTOR_PING:
        {
          uint8_t msg[6] = {0x01, MSG_MOTOR_PING, 0x03, 0x01, 0x00, 0x00};
          if(buffer_receive[i + 3] != 0x01)
            msg[3] = 0x00;

          uint16_t crc_to_send = compute_checksum(msg, 4);
          memcpy(msg + 4, &crc_to_send, sizeof(uint16_t));
          Serial.write(msg, 6);
        }
        break;
      case MSG_MOTOR_STOP:
        {
          uint8_t msg[6] = {0x01, MSG_MOTOR_STOP, 0x03, 0x01, 0x00, 0x00};
          if(buffer_receive[i + 3] != 0x01)
            msg[3] = 0x00;

          uint16_t crc_to_send = compute_checksum(msg, 4);
          memcpy(msg + 4, &crc_to_send, sizeof(uint16_t));
          Serial.write(msg, 6);
          speed_left = no_rotation_left_motor;
          speed_right = no_rotation_right_motor;
          update_motor_speed = true;
        }
        break;
        case MSG_MOTOR_SPEED:
        {
          uint8_t msg[6] = {0x01, MSG_MOTOR_SPEED, 0x03, 0x01, 0x00, 0x00};

          uint16_t crc_to_send = compute_checksum(msg, 4);
          memcpy(msg + 4, &crc_to_send, sizeof(uint16_t));
          Serial.write(msg, 6);
          memcpy(&speed_left, buffer_receive + 3, sizeof(int16_t));
          memcpy(&speed_right, buffer_receive + 3 + sizeof(int16_t), sizeof(int16_t));

          manage_speed();
          update_motor_speed = true;
        }
        break;
    }

     memset(buffer_receive, 0, 255);
     buffer_receive_idx = 0;
}

void send_hit(void)
{
  uint8_t msg[6] = {0x01, MSG_MOTOR_HIT, 0x03, 0x01, 0x00, 0x00};

  uint16_t crc_to_send = compute_checksum(msg, 4);
  memcpy(msg + 4, &crc_to_send, sizeof(uint16_t));
  Serial.write(msg, 6);
}

void manage_speed(void)
{
  speed_left = no_rotation_left_motor + speed_left;
  speed_right = no_rotation_right_motor + speed_right;
  /*if(speed_left < 0)
    speed_left = no_rotation_left_motor + ((180 - no_rotation_left_motor) * speed_left);
  else
    speed_left = no_rotation_left_motor - (no_rotation_left_motor * (speed_left * (-1)));

  if(speed_right < 0)
    speed_right = no_rotation_right_motor + ((180 - no_rotation_right_motor) * speed_right);
  else
    speed_right = no_rotation_right_motor - (no_rotation_right_motor * (speed_right * (-1)));*/
}

void init_crc_16(void)
{
  memset(table_crc, 0, sizeof(table_crc));
  uint16_t value;
  uint16_t temp;
  uint16_t table_crc_length = sizeof(table_crc) / 2;
  for (uint16_t i = 0; i < table_crc_length; ++i)
  {
    value = 0;
    temp = i;
    for (uint8_t j = 0; j < 8; ++j)
    {
      if (((value ^ temp) & 0x0001) != 0)
        value = (uint16_t)((value >> 1) ^ Polynomial);
      else
        value >>= 1;
      temp >>= 1;
    }

    table_crc[i] = value;
  }
}

uint16_t compute_checksum(const uint8_t *bytes, uint32_t size)
{
  return compute_checksum_from_seed(bytes, size, 0);
}

uint16_t compute_checksum_from_seed(const uint8_t *bytes, uint32_t size, uint16_t seed)
{
  uint16_t crc_tmp = seed;

  for (uint32_t i = 0; i < size; ++i)
  {
    uint8_t index = (uint8_t)(crc_tmp ^ bytes[i]);
    crc_tmp = (uint16_t)((crc_tmp >> 8) ^ table_crc[index]);
  }

  return crc_tmp;
}
